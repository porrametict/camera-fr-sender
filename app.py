from app import  create_app
import time 
import  requests 
import threading

HOST = "0.0.0.0"
PORT = 5000



class FaceUpdate(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
            hour =  1
            time_sleep  = hour*60*60
            url =  f"http://{HOST}:{PORT}/update-faces"
            fail_counter = 0 
            while True :
                try:
                    requests.get(url)
                except  :
                    fail_counter += 1 
                
                if fail_counter > 10 :
                    break
                time.sleep(time_sleep)
            
    
face_update = FaceUpdate()
if __name__ == "__main__" :
    app  =create_app()
    face_update.start()
    app.run(debug=True,host=HOST,port=PORT)
    