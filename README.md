# Camera FR Sender
#### Setup Project
1. install dlib
https://gist.github.com/ageitgey/629d75c1baac34dfa5ca2a1928a7aeaf

2.  clone project and create virtual environment
```
git clone https://gitlab.com/porrametict/camera-fr-sender.git
cd  camera-fr-sender
virtualenv venv
source  venv/bin/activate
```
3. install essential modules
```
pip install face_recognition
pip install git+https://gitlab.com/porrametict/face_recognition_classic_blue.git
pip install  -r requirements.txt
```
### run
```
export FLASK_APP=app
flask run
```
### References
[face_recognition](https://pypi.org/project/face-recognition/)
[face_recognotion_classic_blue](https://gitlab.com/porrametict/face_recognition_classic_blue)

