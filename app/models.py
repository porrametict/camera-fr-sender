from . import db 
from flask_login import UserMixin

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))

class IPCamera(db.Model) :
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100))
    uri = db.Column(db.Text)
    status = db.Column(db.Integer)

class ServerUrl(db.Model) :
    id = db.Column(db.Integer,primary_key=True)
    url = db.Column(db.Text)
    status = db.Column(db.Integer)
    
    """
    status 
    0  -> not connect 
    1 -> connected
    """

    def __repr__(self) -> str:
        return '<ServerUrl %r>' % self.id

class FaceGroupEncoding(db.Model) : 
    __tablename__  = "face_group_encodings"
    id = db.Column(db.Integer, primary_key=True)
    sample_encoding = db.Column(db.PickleType())
    profile_id = db.Column(db.Integer)
    profile_name = db.Column(db.String(100))
    version =  db.Column(db.Integer,default=0)

    @property
    def serialize(self):
        return {
            "id" : self.id,
            "version" : self.version
        }

