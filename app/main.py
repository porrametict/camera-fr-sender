from . import db 
from .auth import create_admin
from werkzeug.utils import redirect
from flask.helpers import  url_for
from flask_login import login_required
from .models import  ServerUrl,IPCamera
from flask import Blueprint,render_template
from .face_functions.handle_version_info import  write_version_file

main = Blueprint('main',__name__) 


@main.route('/')
def index():
    return redirect(url_for('main.home'))
    
@main.route('/home')
@login_required
def home():
    camera_set = IPCamera.query.all()
    return render_template('home.html',camera_set=camera_set)

def createServerUrl():
    server_url  = ServerUrl(status=0)
    db.session.add(server_url)
    db.session.commit()
    

@main.route('/reset-db')
def reset_db() : 
    db.drop_all()
    db.create_all()
    createServerUrl()
    write_version_file(0)    
    return create_admin()


