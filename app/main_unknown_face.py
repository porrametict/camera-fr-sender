from flask_login import login_required
from .models import ServerUrl
import requests
from flask import flash,render_template,redirect,url_for,Blueprint

main_unknown_face = Blueprint('main_unknown_face',__name__)
@main_unknown_face.route('/unknown-face')
@login_required
def unknown_face_page():
    image_static_url = None
    try:
        server_url = ServerUrl.query.first()
        get_face_url = server_url.url+'/api/face/unknown-face'
        image_static_url = server_url.url+'/static/image/'
        r= requests.get(get_face_url)
        data =None
        if r.status_code == 200 :
            data = r.json()
            return render_template('unknown_face.html',image_static_url=image_static_url,faces=data) 
    except  :
        flash('Connection Error')
        return render_template('unknown_face.html',image_static_url=image_static_url,faces=[])

@main_unknown_face.route('/clustering-unknow-face')
@login_required
def clustering_unknow_face () :
    server_url = ServerUrl.query.first()
    clustering_url = server_url.url+'/api/face/clustering'
    r= requests.post(clustering_url)
    return redirect(url_for('main_unknown_face.unknown_face_page'))