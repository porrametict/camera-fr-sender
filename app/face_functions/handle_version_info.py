
import os
FILE_NAME = "version-info.txt"
def read_version_file():
    cur_version = 0
    if os.path.exists(FILE_NAME):
        f = open(FILE_NAME,"r")
        current_version = f.readline()
        cur_version = (current_version.split("=")[1])
        f.close()
     
    return cur_version

def delete_version_file():
    if os.path.exists(FILE_NAME):
        os.remove(FILE_NAME)


def write_version_file(version=0):
        f = open(FILE_NAME,"w")
        f.write(f"VERSION={version}")
        f.close()

    
if __name__ == "__main__" :
    write_version_file(3)