import random
import math 
import requests
import string
import datetime
from ..config import  min_face_width_threshold,min_face_height_threshold
from face_recognition_classic_blue import cv_image_to_base64 as opencv_frame_to_baseb4
def get_random_colour() : 
    r = random.randint(0,255)
    g = random.randint(0,255)
    b = random.randint(0,255)
    return (r,g,b)

def get_random_string(length) :
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def min_diff (curr_location,locations) : 
    """
    Return minimun distance between current location and anothor location
    """
    mindist = math.dist(curr_location,locations[0])
    for location in locations : 
        dist  = math.dist(curr_location,location)
        if dist < mindist : 
            mindist = dist 
    return mindist


def diff_locations (pre_locations,locations): 
    """
    Return a list of minimun distances location and pre_locations   
    """
    
    diff = []
    if len(pre_locations) > 0  : 
        for cur_location in locations : 
            mindiff = min_diff(cur_location,pre_locations)
            diff.append(mindiff)
    return diff 

def post_image(json_data,API_ENDPOINT) : 
    """
    Post face image to  server 
    """
    r  = requests.post(url=API_ENDPOINT,json=json_data)
    if r.ok : 
        return True 
    else :
        return False

def prepare_data_post(frame) :
    b4_frame_string = opencv_frame_to_baseb4(frame)
    return {
            # "encoding" : list(),
            "image" : b4_frame_string.decode("utf-8"),
            "timestamp" :  str(datetime.datetime.now())
        }


def filter_face_locations (face_locations) :
    """
    Return face_encodings that have width more than 100 points  and heigth more than 100 points
    """
    min_width_threshold = min_face_width_threshold
    min_height_threshold = min_face_height_threshold
    new_locations  = []

    for  face_location  in  face_locations :
        (top, right, bottom, left) = face_location
        if  (abs(top - bottom) < min_height_threshold ) or (abs(left - right ) < min_width_threshold ) : 
            pass 
        else : 
            new_locations.append(face_location)
    return new_locations
    