import cv2 
import face_recognition
from .easy_funcs import *  
from .easy_compare import *
from .load_data import  get_known_faces
from ..config import  TOLERANCE
from face_recognition_classic_blue.get_rtsp_stream import get_rtsp_media
from  face_recognition_classic_blue import  predict_faces
from PIL import ImageFont,ImageDraw,Image


def putText(img,p1,p2,text) : 
    """
    put text to frame
    """
    fontpath = "app/face_functions/TH Niramit AS.ttf"
    font = ImageFont.truetype(fontpath,45)
    img_pill = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pill)
    draw.text((p1,p2),text,font=font,fill=(255,255,255,1))
    return np.array(img_pill)

def generate(src) :
    capture  = get_rtsp_media(src) # get rtsp stream

    # const
    known_face_encodings, known_face_group_ids,known_face_names  = get_known_faces()

    # variable
    process_this_frame = True
    
    # current face_name on frame
    face_group_names = []


    while True :
        ret,frame = capture.read()
        if not ret : 
            print("media is not avaliable.")
            break
            
        # process 
        small_frame = cv2.resize(frame,(0,0),fx=0.5,fy=0.5)
        rgb_small_frame = cv2.cvtColor(small_frame,cv2.COLOR_BGR2RGB)
        
        if process_this_frame : 
            # get face location and  encodings
            face_locations = face_recognition.face_locations(rgb_small_frame)
            face_locations = filter_face_locations(face_locations)
            face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
            # predict face from face_encodings
            face_group_names = predict_faces(known_face_names,known_face_encodings,face_encodings,tolerance=TOLERANCE)
            # change None to Unknown for display 
            face_group_names = [ name if name is not  None  else "Unknown" for name in face_group_names] 


        process_this_frame = not process_this_frame
        
        for(top,right,bottom,left),face_group_id in zip(face_locations,face_group_names) : 

            top *= 2
            right *= 2
            bottom *= 2
            left *= 2
            line_colour = (0,0,255)
            #  draw box
            cv2.rectangle (frame,(left,top),(right,bottom),line_colour,3)
            cv2.rectangle(frame,(left,bottom-35),(right,bottom),line_colour,cv2.FILLED)
            # display name 
            frame = putText(frame,left +6 ,bottom-40,face_group_id)
        # display current time 
        timestamp = datetime.datetime.now()
        cv2.putText(frame, timestamp.strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

        outputFrame = frame.copy()

        if outputFrame is None :
            continue
        (flag,encodedImage) = cv2.imencode(".jpg",outputFrame)    
        if not flag :
            continue
        yield (b'--frame\r\n' b'Content-Type : image/jpeg\r\n\r\n' + bytearray(encodedImage) + b'\r\n')
    capture.release()
