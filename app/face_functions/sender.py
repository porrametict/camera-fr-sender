import cv2 
from  face_recognition_classic_blue.get_rtsp_stream import get_rtsp_media
import face_recognition
from .easy_funcs import *  
from .easy_compare import *
import requests

def update_camera_status(url,status) :
    payload = {
        'status' : status
    }
    r = requests.post(url,json=payload)


def write_post(frame,server_api) :
    """
    POST face image to server
    """
    post_data = prepare_data_post(frame)
    post_image(post_data,server_api)

def process_stream(rtsp_uri : str , server_api : str,update_camera_url):
    """
    process image and find face then send image to server
    """
    
    media_capture = get_rtsp_media(rtsp_uri)  

    # const
    _font = cv2.FONT_HERSHEY_COMPLEX
    same_face_location_distance = 50
    face_similar_percent = 30

    # variable
    process_this_frame = True
    pre_face_locations = []
    pre_frame = None


    cur_face_locations = []
    cur_diff_locations = []

        
    update_camera_status(update_camera_url,1)

    while True : 
        ret,frame = media_capture.read()
        if not ret : 
            # empty frame
            break
        # process 
        
        #  reszie frame and covert frame to RGB  (face_recognition use RGB)
        small_frame = cv2.resize(frame,(0,0),fx=0.5,fy=0.5)
        rgb_frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        rgb_small_frame = cv2.cvtColor(small_frame,cv2.COLOR_BGR2RGB)
        
        # process every anothor frame 
        if process_this_frame : 
            pre_face_locations = cur_face_locations 
            
            # find face location in images  
            cur_face_locations = face_recognition.face_locations(rgb_small_frame)
            # filter face location that have enough face size to process
            cur_face_locations  = filter_face_locations(cur_face_locations)
            # find  distance  between previous locations and  current  locations   
            cur_diff_locations = diff_locations(pre_face_locations,cur_face_locations)

            # check this frame is not frist frame
            if pre_frame is not None : 
                
                for i in range(len(cur_diff_locations)) :
                    """
                     Concept :  for ipcam -> current face_location (this frame) and  previous locations (previous frame) that  nearest .it might same face   
                    """
                    # check current_distance under distance same face threshold
                    if cur_diff_locations[i] <  same_face_location_distance and  len(pre_face_locations) > i:
                        
                        # make sure this location are same face | compare between this frame and previos frame (nearest locations)
                        similar_precent =  compare_face_image(rgb_small_frame,cur_face_locations[i],pre_frame,pre_face_locations[i])
                        if similar_precent > face_similar_percent : 
                                continue
                    #   draw time stamp
                    timestamp = datetime.datetime.now()
                    cv2.putText(frame, timestamp.strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 2)
                    # put to server
                    write_post(frame=rgb_frame,server_api=server_api)

            pre_frame = rgb_small_frame    
        
        process_this_frame = not process_this_frame
            
    media_capture.release()
    update_camera_status(update_camera_url,0)
        
if __name__ == "__main__":
    process_stream()