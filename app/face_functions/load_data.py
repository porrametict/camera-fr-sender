import sqlalchemy as db 
import pickle
KNOWN_FACE_ENCODINGS = []
KNOWN_FACE_GROUP_IDS = []
KNOWN_FACE_NAMES= []

def connect_face_table():
    engine = db.create_engine("sqlite:///app/db.sqlite")
    connection = engine.connect()
    metadata = db.MetaData()
    face_encoding_table = db.Table("face_group_encodings",metadata,autoload=True,autoload_with=engine)
    return connection,face_encoding_table

def query_faces(conn,face_table):
    query = db.select([face_table])
    result_proxy = conn.execute(query)
    result_set = result_proxy.fetchall()
    for i in result_set :
        KNOWN_FACE_ENCODINGS.append(pickle.loads(i.sample_encoding))
        KNOWN_FACE_GROUP_IDS.append(i.profile_id)
        KNOWN_FACE_NAMES.append(i.profile_name)

    
def get_known_faces():
    if len(KNOWN_FACE_ENCODINGS) and len(KNOWN_FACE_GROUP_IDS) :
        return  KNOWN_FACE_ENCODINGS,KNOWN_FACE_GROUP_IDS,KNOWN_FACE_NAMES
    else :
        conn,face_table =  connect_face_table()
        query_faces(conn,face_table)
        # print(KNOWN_FACE_GROUP_IDS)
        return  KNOWN_FACE_ENCODINGS,KNOWN_FACE_GROUP_IDS,KNOWN_FACE_NAMES

        
    