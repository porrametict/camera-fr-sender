import cv2 
import numpy as np
import time 
import os 
from .easy_funcs import get_random_string
sift = cv2.xfeatures2d.SIFT_create()

current_path = os.getcwd()
UPLOAD_DIRECTORY = os.path.join(current_path,"save_v6/")
save_image_path = os.path.join(current_path,UPLOAD_DIRECTORY)



def findImageSimilar(image1,image2):
    """
    Compare two image and retrun percent of similarity
    """
    try :
        kp_1,desc_1 = sift.detectAndCompute(image1,None)
        kp_2,desc_2  = sift.detectAndCompute(image2,None)

        index_params = dict(algorithm=0,trees=5)
        search_params = dict()
        flann = cv2.FlannBasedMatcher(index_params,search_params)
        matchs = flann.knnMatch(desc_1,desc_2,k=2)

        good_point = []
        ratio = 0.7
        for m,n in matchs:
            if m.distance < ratio*n.distance : 
                good_point.append(m)
        
        number_keypoints = 0
        if len(kp_1) <= len(kp_2):
            number_keypoints = len(kp_1)
        else : 
            number_keypoints = len(kp_2)

        if number_keypoints != 0 :
            percent_match = len(good_point) / number_keypoints *100
        else : 
            percent_match = 0
        return percent_match
    except : 
        return 0 

def crop_face(frame,face_location) :
    (top,right,bottom,left) = face_location
    w = abs(right -left)
    h = abs(top - bottom)

    crop_image = frame[top:(top+h),left:(left+w)]
    crop_image = cv2.cvtColor(crop_image,cv2.COLOR_BGR2RGB)
    return crop_image


def compare_face_image(frame1,face_location1,frame2,face_location2,isSaveImageCompare=False)  :
    """
    Compare two image faces
    """
    face1 = crop_face(frame1,face_location1)
    face2 = crop_face(frame2,face_location2)

    face_similar_percent = findImageSimilar(face1,face2)
 
    return face_similar_percent


def find_face_locations_distances(one_face_location,face_locations) :
    """
    Return distance between  two  face_locatio
    """
    locations_distances = []
    arr_one_face_localtion = np.array(one_face_location)
    for face_location in face_locations :
        arr_temp_face_location = np.array(face_location)
        location_distance =  sum((arr_one_face_localtion-arr_temp_face_location)**2)
        locations_distances.append(location_distance)
    return locations_distances



def find_nested_face_distance_index(one_face_location,face_locations) : 
    """
    Return nearest distance between face location and anothor face locations 
    """
    if len(face_locations) > 0 :
        locations_distances  = find_face_locations_distances(one_face_location,face_locations)
        nested_match_index  = np.argmin(locations_distances)
        return nested_match_index
    else : 
        return -1
