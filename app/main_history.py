import json
import requests
from werkzeug.utils import redirect
from flask.helpers import flash, url_for
from flask_login import login_required
from .models import  ServerUrl
from flask import render_template,request,Blueprint

main_history = Blueprint("main_history",__name__)

@main_history.route('/history')
@login_required
def history_page() :
    page =  request.args.get('page',1)
    payload = request.args.get('payload',{})
    
    if len(payload) :
        payload = payload.replace("\'","\"")
        payload = json.loads(payload)
        if "page" in payload :
            del payload['page']
    payload["page"] = page 
    data = None
    image_static_url = None
    try :
        server_url = ServerUrl.query.first()
        image_static_url = server_url.url+'/static/image/'
        get_face_url = server_url.url+'/api/face/face'
        r= requests.get(get_face_url,params=payload)
        if r.status_code == 200 :
            data = r.json()
        else :
            data = None
            flash("Connetion Error")
    except  : 
        flash("Connetion Error")

    return render_template('history.html',image_static_url=image_static_url,data=data,payload=payload) 

@main_history.route('/history/change')
@login_required
def history_change_page() :
    page =  request.args.get('page',1)
    payload = request.args.get('payload',{})
    payload = payload.replace("\'","\"")
    payload = json.loads(payload)
    if "page" in payload :
            del payload['page']
    if not len(payload) :
        payload = None
    return redirect(url_for('main_history.history_page',payload=payload,page=page))

@main_history.route('/history',methods=['POST'])
@login_required
def history_page_post() : 
    payload = dict()
    name = request.form.get('filter_name') if request.form.get('filter_name') != '' else None
    start_datetime = request.form.get('start_datetime') if request.form.get('start_datetime') != '' else None
    end_datetime = request.form.get('end_datetime') if request.form.get('end_datetime') != '' else None
    if name is not None:
        payload['name'] =  name
    if start_datetime is not None :
        payload['start_datetime'] = start_datetime
    if end_datetime is not None :
        payload['end_datetime'] = end_datetime
    data = None
    if len(payload):
        data = json.dumps(payload)
    return redirect(url_for('main_history.history_page',payload=data,page=1))

