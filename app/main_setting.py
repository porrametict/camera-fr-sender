import requests
from . import db 
from urllib.request import urlopen
from werkzeug.utils import redirect
from flask.helpers import flash, url_for
from flask_login import login_required
from .models import  ServerUrl,FaceGroupEncoding
from flask import render_template,request,Blueprint
from .face_functions.handle_version_info import  write_version_file,read_version_file
import numpy as np

main_setting  = Blueprint('main_setting',__name__)
@main_setting.route('/setting')
@login_required
def setting () :
    face_version = int(read_version_file())
    server_url = ServerUrl.query.first()
    checking_url = server_url.url+"/api/face/face-group-version" if server_url.url else None
    face_server_version = None
    if checking_url :
        try: 
            r = requests.get(checking_url)
            data = r.json()
            face_server_version = data['version']
        except :
            flash("Connection Error")

    return  render_template("setting.html",server=server_url,face_version = face_version,face_server_version = face_server_version)


@main_setting.route('/setting/server',methods=['POST'])
@login_required
def setting_server_post() :
    server_url = request.form.get('server_url')
    url_res = None
    try :
        url_res =  urlopen(server_url)
    except :
        flash("Connection Failure")

    if url_res is not None : 
        server_object =  ServerUrl.query.first()
        server_object.url = server_url
        server_object.status =  1 
        db.session.commit()
    
    return redirect(url_for('main_setting.setting'))

@main_setting.route('/load-faces')
def load_face() : 
    try :
        server_url = ServerUrl.query.first()
        load_face_url = server_url.url+'/api/face/face-group-encoding'
        r= requests.get(load_face_url)
        if r.ok :
            data = r.json()
            groups = data['groups_encoding']
            version = data['version']
            for f in groups :
                new_face_group = FaceGroupEncoding(profile_id=f['profile_id'],profile_name=f['profile_name'],sample_encoding= np.array(f['sample_encoding']) ,version=f['version'])
                db.session.add(new_face_group)
                db.session.commit()            
            write_version_file(version)
        else :
            data = None
            flash("Connetion Error")
    except  : 
        flash("Connetion Error")

    return redirect(url_for("main.setting"),)
    


@main_setting.route('/update-faces')
def update_face():
    faces = FaceGroupEncoding.query.all()
    faces = [face.serialize for face in faces]
    try: 
        server_url = ServerUrl.query.first()
        load_face_url = server_url.url+'/api/face/face-group-encoding'
        r = requests.post(load_face_url,json={"face_group_encoding":faces})
        data = r.json()
        groups = data['groups_encoding']
        version  = data['version']
        for f in groups :
            update_type = f['update_type']
            if update_type == "new" :
                new_face_group = FaceGroupEncoding(profile_id=f['profile_id'],profile_name=f['profile_name'],sample_encoding= np.array(f['sample_encoding']) ,version=f['version'])
                db.session.add(new_face_group)
                db.session.commit()            
            elif update_type == "update" :
                face_group  = FaceGroupEncoding.query.get(f['id'])
                face_group.profile_id = f['profile_id']
                face_group.profile_name=f['profile_name']
                face_group.sample_encoding = f['sample_encoding']
                face_group.version = f['version']
            elif update_type == "remove" : 
                face_group  = FaceGroupEncoding.query.get(f['id'])
                db.session.delete(face_group)
                db.session.commit()
        write_version_file(version)
    except :
        flash("Connection Error")
    return redirect(url_for('main_setting.setting'))
                

