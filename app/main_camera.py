
import threading
from . import db 
import urllib.parse
from werkzeug.utils import redirect
from flask.helpers import flash, url_for
from .face_functions.webstraming import generate
from flask_login import login_required
from .models import  ServerUrl,IPCamera
from flask import render_template,request,Response,Blueprint
from .face_functions.sender import  process_stream as process_stream_v2


main_camera = Blueprint('main_camera',__name__)

@main_camera.route('/camera/add')
@login_required
def add_camera():
    return render_template('ipcamera/add_camera.html')

@main_camera.route('/camera/add',methods=['POST'])
@login_required
def add_camera_post() :
    rtsp_uri = request.form.get('rtsp_uri') 
    camera_name  = request.form.get('camera_name')
    server_url = ServerUrl.query.first()
    new_camera = IPCamera(name=camera_name,uri=rtsp_uri,status=0)
    db.session.add(new_camera)
    db.session.commit()
    update_camera_url = str(request.host_url)[:-1]+ url_for('main_camera.update_camera_status',id=new_camera.id)
    
    post_image_url = server_url.url+"/api/image"
    
    t1 = threading.Thread(target=process_stream_v2,args=(rtsp_uri,post_image_url,update_camera_url ))
    t1.start()
    return redirect(url_for("main.home"))

@main_camera.route('/camera/edit/<int:id>')
@login_required
def edit_camera(id) :
    camera = IPCamera.query.get(id)
    return render_template('ipcamera/edit_camera.html',camera=camera)

@main_camera.route('/camera/edit<int:id>',methods=['POST'])  # incomplete
@login_required
def edit_camera_post(id) :
    rtsp_uri = request.form.get('rtsp_uri') 
    camera_name  = request.form.get('camera_name')
    camera = IPCamera.query.get(id)
    camera.uri = rtsp_uri
    camera.name = camera_name
    camera.status = 0 

    db.session.commit()
    
    return render_template('ipcamera/edit_camera.html',camera=camera)

@main_camera.route("/camera-api/update-camera-stutus/<int:id>",methods=['POST'])    
def update_camera_status(id):
    content = request.get_json(silent=True)
    camera = IPCamera.query.get(id)
    status = content['status']
    camera.status = status
    db.session.commit()
    return "success"

@main_camera.route("/camera/reconnect/<int:id>") 
@login_required
def connect_camera (id):
    camera = IPCamera.query.get(id)
    rtsp_uri = camera.uri
    server_url = ServerUrl.query.first()
    post_image_url = server_url.url+"/api/image"
    update_camera_url = str(request.host_url)[:-1]+ url_for('main_camera.update_camera_status',id=id)
    t1 = threading.Thread(target=process_stream_v2,args=(rtsp_uri,post_image_url,update_camera_url ))
    t1.start()
    return redirect(url_for('main_camera.edit_camera',id=camera.id))

@main_camera.route("/camera/view-media/")
@login_required
def display_media() :
    source = request.args.get('source')
    return render_template("ipcamera/view_media.html",source=source)

@main_camera.route("/camera/get-media/")
def get_media() :
    source = request.args.get('source')
    source = urllib.parse.unquote(source)
    # print("source",source)
    return Response(generate(source),mimetype = "multipart/x-mixed-replace; boundary=frame")


@main_camera.route("/camera/delete/<int:id>") 
@login_required
def delete_camera(id):
    camera = IPCamera.query.get(id)
    db.session.delete(camera)
    db.session.commit()    
    return redirect(url_for("main.home"))
