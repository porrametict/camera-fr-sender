import json
import requests
import base64
from werkzeug.utils import redirect
from flask.helpers import flash, url_for
from .models import  ServerUrl
from flask import render_template,request,Blueprint
from flask_login import login_required

main_face =  Blueprint('main_face',__name__)

@main_face.route('/face')
@login_required
def face_page() :
    page = request.args.get('page',1)
    name = request.args.get('name',None)
    payload = request.args.get('payload',{})

    if len(payload) : 
        payload = payload.replace("\'","\"")
        payload = json.loads(payload)
        if "page" in payload :
            del payload['page']
    payload['page'] = page
    
    if name and name.strip() != "" :
        payload['name'] = name 
    data = None
    try:
        server_url = ServerUrl.query.first()
        face_url = server_url.url+"/api/face/face-group"
        r= requests.get(face_url,params=payload)
        if r.status_code == 200 :
            data = r.json()
        else :
            flash("Connection Error")
    except  : 
        flash("Connection Error")
    return render_template('face.html',data=data,payload=payload)

@main_face.route('/face/change')
@login_required
def face_change_page() :
    page =  request.args.get('page',1)
    payload = request.args.get('payload',{})
    payload = payload.replace("\'","\"")
    payload = json.loads(payload)
    if "page" in payload :
            del payload['page']
    if not len(payload) :
        payload = None
    return redirect(url_for('main_face.face_page',payload=payload,page=page))

@main_face.route('/face/add')
@login_required
def add_face() : 
    return render_template('face/add_face.html')

@main_face.route('/face/add',methods=['POST'])
@login_required
def add_face_post():
    face_image = request.files.get('face_image')
    face_name = request.form.get('name')
    face_image64 = base64.b64encode(face_image.read())
    server_url = ServerUrl.query.first()
    post_face_url = server_url.url+'/api/face/face-group'
    r = requests.post(post_face_url,json={
        'name' : face_name,
        'image' : face_image64.decode()
    })
    if (r.status_code == 200 ):
        return redirect(url_for('main_face.face_page'))
    flash('Connection Error')
    return redirect(url_for('main_face.add_face'))

@main_face.route('/face/<int:id>')
@login_required
def face_page_view(id) :
    server_url = ServerUrl.query.first()
    get_face_url = server_url.url+'/api/face/face-group/'+str(id)
    r= requests.get(get_face_url)
    data =None
    image_static_url = server_url.url+'/static/image/'
    if r.status_code == 200 :
        data = r.json()
        return render_template('face/face_view.html',face_group=data,image_static_url=image_static_url) 
    flash('Connection Error')
    return render_template('face/face_view.html',face_group={},image_static_url=image_static_url)


@main_face.route('/face/<id>/approve')
def approve_page (id) :
    page =  request.args.get('page',1)
    payload = request.args.get('payload',{})
    
    if len(payload) :
        payload = payload.replace("\'","\"")
        payload = json.loads(payload)
        if "page" in payload :
            del payload['page']
    payload["page"] = page 
    data = None
    image_static_url = None
    try :
        server_url = ServerUrl.query.first()
        image_static_url = server_url.url+'/static/image/'
        get_face_url = server_url.url+'/api/face/face-list-approve'
        r= requests.get(get_face_url,params=payload)
        if r.status_code == 200 :
            data = r.json()
        else :
            data = None
            flash("Connetion Error")
    except  : 
        flash("Connetion Error")
    return render_template('/face/approve_page.html',image_static_url=image_static_url,data=data,payload=payload) 


@main_face.route("/approve-face",methods=['POST'])
def approve_face():
    json_data = request.get_json()
    try:
        server_url = ServerUrl.query.first()
        approve_face_url = server_url.url +'/api/face/face-approve'
        r  = requests.post(approve_face_url,json=json_data)
        data = r.json()
    except  :
        flash ("Connection Error")
    return  redirect(url_for("main.face_page_view",id=json_data['face_group_id']))