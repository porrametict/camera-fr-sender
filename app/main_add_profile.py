import requests
import base64
from werkzeug.utils import redirect
from flask.helpers import  url_for
from flask_login import login_required
from .models import  ServerUrl
from flask import render_template,request,Blueprint

main_add_profile = Blueprint('main_add_profile',__name__) 

@main_add_profile.route("/add-profile")
@login_required
def add_profile():
    return render_template('add_profile.html')


@main_add_profile.route("/add-profile",methods=['POST'])
def add_profile_post() :
    json_data = dict()
    csv_data = request.files['data_csv']
    
    json_data['csv_data'] =  list( base64.b64encode( csv_data.read()))

    json_data['images'] =  []
    

    image_files = request.files.getlist("data_images")
    for  image in image_files : 
        image_data = {
            "file_name" :  image.filename,
            "base64" : list( base64.b64encode(image.read()) )
        }
        json_data['images'].append(image_data)

    try:
        server_url = ServerUrl.query.first()
        profile_url = server_url.url + "/api/profile"
        r = requests.post(profile_url,json=json_data)
    except :
        pass 
    return redirect(url_for('main.home'))
    