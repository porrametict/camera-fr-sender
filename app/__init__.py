from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager


# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()

def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'secret-key-goes-here'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

    db.init_app(app)
    login_manager = LoginManager()



    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)


    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))


    # blueprint for auth routes in our app
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    from .main_add_profile import main_add_profile
    from .main_camera import main_camera
    from .main_face import main_face
    from .main_history import main_history
    from .main_setting import main_setting
    from .main_unknown_face import main_unknown_face
    
    
    app.register_blueprint(main_blueprint)
    app.register_blueprint(main_add_profile)
    app.register_blueprint(main_camera)
    app.register_blueprint(main_face)
    app.register_blueprint(main_history)
    app.register_blueprint(main_setting)
    app.register_blueprint(main_unknown_face)


    from .template_filters import (
        get_camera_status,
        get_timestamp_format,
        get_face_name_or_unknown,
        generate_page_range,
        )
    
    app.jinja_env.filters['get_camera_status'] = get_camera_status
    app.jinja_env.filters['get_timestamp_format'] = get_timestamp_format
    app.jinja_env.filters['get_face_name_or_unknown'] = get_face_name_or_unknown
    app.jinja_env.filters['generate_page_range']  = generate_page_range
    return app