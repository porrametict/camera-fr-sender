from datetime import datetime
import math 
def get_camera_status(status) :
    if(status == 0 ) :
        return "Offline"
    elif (status == 1) :
        return "Online"
    

def get_timestamp_format(timestamp :str ) :
    date_time_obj = datetime.strptime(timestamp, '%Y-%b-%d %I:%M %p')
    return  date_time_obj.strftime("%d/%m/%Y, %H:%M:%S")

def get_face_name_or_unknown(face) :
    if face['group'] :
        return face['group']['profile']['given_name_th']
    return f"Unknown_{face['id']}"


def generate_page_range(data) :
    pages = data['pages'] +1
    page = data['page']
    display_page = 7
    half_display_page = math.floor(display_page/2)
    
    
     
    if pages <= display_page :
        return range(1,pages)
    else : 
        last_pages = range(pages,pages-display_page-1,-1)
        if page <=  7 :
            return range(1,display_page+1)
        elif  page in last_pages:
            print(last_pages[-1],pages)
            return range(last_pages[-1],pages,1)
        else :  
            print('else else ')
            start_page =  page -half_display_page if page - half_display_page  >=1 else 1
            end_page = page+ (half_display_page+1) if page + (half_display_page+1) <= pages else pages 
            return range(start_page,end_page)