from flask import Blueprint,redirect,url_for,flash,request
from flask.templating import render_template
from werkzeug.security import generate_password_hash,check_password_hash
from . import db 
from .models import User
from flask_login import login_user, logout_user, login_required

auth  = Blueprint ('auth',__name__)


@auth.route("/login")
def login() :
    return render_template("login.html")

@auth.route('/login',methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remenber') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password,password) :
        flash ("Plase check your login details and try agin.")
        return redirect(url_for('auth.login'))

    login_user(user, remember=remember)
    return redirect(url_for('main.home'))



@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


# @auth.route('/create_admin')
def create_admin  ():
    email = 'admin@mail.com'
    name = 'admin'
    password = "password"
    
    user = User.query.filter_by(email=email).first()
    if user: # if a user is found, we want to redirect back to signup page so user can try again
        # flash('Email address already exists')
        return {"message" : "Email already exists"}

    new_user = User(email=email,name=name,password=generate_password_hash(password,method='sha256'))

    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))
    

